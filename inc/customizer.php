<?php 
function codepopular_customize_register( $wp_customize ) 
{
  
  /***********************************************
               Generel setting
  ************************************************/
  $wp_customize->add_section( 'dostart_new_section' , [
        'title'    => __( 'Generel Settings', 'dostart' ),
        'priority' => 1
    ]); 


     $wp_customize->add_setting(
     'dostart_primary_color' ,
      array(
        'default'   => '#0052a5',
        'transport' => 'refresh',
         'sanitize_callback'    => 'vr_sanitize_html'
    ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dostart_primary_color',
     array(
        'label'    => __( 'Primary Color', 'dostart' ),
        'section'  => 'dostart_new_section',
        'settings' => 'dostart_primary_color',
        'type'     => 'color'
    ) ) );


    /***********************************************
                Header settings
    ************************************************/
   $wp_customize->add_section( 'dostart_header_section' , [
        'title'    => __( 'Header', 'dostart' ),
        'priority' => 2
    ]); 
      // header background
     $wp_customize->add_setting(
     'dostart_header_bg' ,
      array(
        'default'   => '#ffffff',
        'transport' => 'refresh',
            'sanitize_callback'    => 'vr_sanitize_html'
    ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dostart_header_bg',
     array(
        'label'    => __( 'Header Background', 'dostart' ),
        'section'  => 'dostart_header_section',
        'settings' => 'dostart_header_bg',
        'type'     => 'color'
    ) ) );  

    // Logo Text Color 
     $wp_customize->add_setting(
     'dostart_logo_textcolor' ,
      array(
        'default'   => '#000000',
        'transport' => 'refresh',
         'sanitize_callback'    => 'vr_sanitize_html'
    ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dostart_logo_textcolor',
     array(
        'label'    => __( 'Logo Text Color', 'dostart' ),
        'section'  => 'dostart_header_section',
        'settings' => 'dostart_logo_textcolor',
        'type'     => 'color'
    ) ) );


  /*************************************************
              Footer settings
  ************************************************/

   $wp_customize->add_section( 'dostart_footer_section' , [
        'title'    => __( 'Footer', 'dostart' ),
        'priority' => 3
    ]); 

   // Footer Background
     $wp_customize->add_setting(
     'dostart_footer_bg' ,
      array(
        'default'   => '#1b1b1b',
        'transport' => 'refresh',
         'sanitize_callback'    => 'vr_sanitize_html'
    ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dostart_footer_bg',
     array(
        'label'    => __( 'Footer Background', 'dostart' ),
        'section'  => 'dostart_footer_section',
        'settings' => 'dostart_footer_bg',
        'type'     => 'color'
    ) ) );

      

 

}

add_action( 'customize_register', 'codepopular_customize_register');


function vr_sanitize_html( $html ) {
    return wp_filter_post_kses( $html );
}
